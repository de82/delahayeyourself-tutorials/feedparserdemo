from flask import Flask, render_template
import feedparser


app = Flask(__name__)

feeds_url = (
    'https://scghost.judgehype.com/nouvelles.xml',
    'http://feeds.feedburner.com/PythonInsider',
    'https://www.raspberrypi.org/blog/feed/',
    'https://starcraft2.judgehype.com/nouvelles.xml',

)


@app.route('/')
def index():
    feeds = [
        feedparser.parse(url)
        for url in feeds_url
    ]
    return render_template('feed.html', feeds=feeds)
